delimiter //
CREATE PROCEDURE simpleproc (OUT param1 INT)
begin
    declare test int;
    select test;
    select count(*) into param1 from student;
    select count(*) into test from student;
    select test;
end//
delimiter ;

call simpleproc(@a);

select @a;

drop PROCEDURE simpleproc;
