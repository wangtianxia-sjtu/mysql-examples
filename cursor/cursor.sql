delimiter //
create procedure cursordemo()
begin
    DECLARE done INT DEFAULT FALSE;
    DECLARE cdept_name varchar(20);
    DECLARE csalary decimal(8,2);
    DECLARE total_salary decimal(20,2) DEFAULT 0.0;
    declare cur CURSOR for select dept_name, salary from instructor;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    open cur;

    read_loop: LOOP
    fetch next from cur into cdept_name, csalary;
    if done then
        leave read_loop;
    end if;
    select cdept_name;
    select csalary;
    set total_salary = total_salary + csalary;
    end LOOP;
    select total_salary;
    close cur;
end//
delimiter ;

call cursordemo();

drop procedure cursordemo;