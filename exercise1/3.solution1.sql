drop table if exists scholarship_temp;
drop procedure if exists check_scholarship;

create temporary table scholarship_temp
    (
        dept_name		varchar(20),
        level           char(1),
        s_ID			varchar(5),
        s_name			varchar(20),
        A_num           int
    );

-- select ID, name, dept_name, sum(grade='A+' or grade='A') as 'A/A+', sum(grade like 'A%') as 'A class', sum(grade like 'C%') as 'C class' from takes natural join student group by ID;

DELIMITER //
create procedure check_scholarship ()
begin
    DECLARE done INT DEFAULT FALSE;
    declare this_ID varchar(5);
    declare this_name varchar(20);
    declare this_dept_name varchar(20);
    declare this_a_aplus_num int;
    declare this_a_class_num int;
    declare this_c_class_num int;
    declare cur CURSOR for select ID, name, dept_name, sum(grade='A+' or grade='A') as 'A/A+', sum(grade like 'A%') as 'A class', sum(grade like 'C%') as 'C class' from takes natural join student group by ID;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    open cur;

    read_loop: loop
    fetch from cur into this_ID, this_name, this_dept_name, this_a_aplus_num, this_a_class_num, this_c_class_num;
    if done then
        leave read_loop;
    end if;
    if this_a_aplus_num >= 2 and this_c_class_num = 0 then
        -- A level scholarship
        insert into scholarship_temp values (this_dept_name, 'A', this_ID, this_name, this_a_class_num);
        iterate read_loop;
    end if;
    if this_a_aplus_num = 1 and this_c_class_num = 0 then
        -- B level scholarship
        insert into scholarship_temp values (this_dept_name, 'B', this_ID, this_name, this_a_class_num);
        iterate read_loop;
    end if;
    end loop;
    close cur;
end//
DELIMITER ;

call check_scholarship();

select * from scholarship_temp order by dept_name, level, A_num desc;

drop table scholarship_temp;
drop procedure check_scholarship;
