delimiter //
create function teacher_salary (this_dept_name varchar(20))
returns decimal(20,2)
begin
    DECLARE done INT DEFAULT FALSE;
    declare total decimal(20,2) default 0.0;
    declare cur_dept_name varchar(20);
    declare cur_salary decimal(8,2);
    declare cur CURSOR for select dept_name, salary from instructor;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
    open cur;

    read_loop: loop
    fetch from cur into cur_dept_name, cur_salary;
    if done then
        leave read_loop;
    end if;
    if cur_dept_name = this_dept_name then
        set total = total + cur_salary;
    end if;
    end loop;
    close cur;
    return total;
end//
delimiter ;

-- select teacher_salary('English');

select dept_name, teacher_salary(dept_name) from department;

drop function teacher_salary;