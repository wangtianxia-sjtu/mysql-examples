-- List the student ID and the number of his/her 'A+'s, 'A's and so on.

select ID, sum(grade='A+') as 'A+', sum(grade='A') as 'A',sum(grade='A-') as 'A-', sum(grade='B+') as 'B+',
sum(grade='B') as 'B', sum(grade='B-') as 'B-', sum(grade='C+') as 'C+', sum(grade='C') as 'C',
sum(grade='C-') as 'C-' from takes natural join student group by ID;