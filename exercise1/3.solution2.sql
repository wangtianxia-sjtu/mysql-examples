-- This SQL script is NOT finished!

drop table if exists scholarship_temp;
drop procedure if exists check_scholarship;
drop function if exists check_A;
drop function if exists check_B;

create temporary table scholarship_temp
    (
        dept_name		varchar(20),
        level           char(1),
        s_ID			varchar(5),
        s_name			varchar(20),
        A_num           int
    );

delimiter //
CREATE PROCEDURE check_scholarship ()
begin
    DECLARE done INT DEFAULT FALSE;
    declare this_student_id varchar(5);
    declare this_student_name varchar(20);
    declare this_dept_name varchar(20);
    declare aclass_num int;
    declare cur CURSOR for select ID, name, dept_name from student;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    open cur;

    read_loop: loop
    fetch from cur into this_student_id, this_student_name, this_dept_name;
    if done then
        leave read_loop;
    end if;
    set aclass_num = check_A(this_student_id);
    if aclass_num > 0 then
        insert into scholarship_temp values (this_dept_name, 'A', this_student_id, this_student_name, aclass_num);
        iterate read_loop;
    end if;
    set aclass_num = check_B(this_student_id);
    if aclass_num > 0 then
        insert into scholarship_temp values (this_dept_name, 'B', this_student_id, this_student_name, aclass_num);
    end if;
    end loop;
    close cur;
end//
delimiter ;

delimiter //
create function check_A (this_student_id varchar(5))
returns int
begin
    DECLARE done INT DEFAULT FALSE;
    declare this_a_aplus_num int;
    declare this_a_class_num int;
    declare this_c_class_num int;
    declare this_grade varchar(2);

    declare cur CURSOR for select grade from takes natural join student where ID = this_student_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
    set this_a_aplus_num = 0;
    set this_a_class_num = 0;
    set this_c_class_num = 0;

    open cur;

    read_loop: loop
        fetch from cur into this_grade;

        if done then
            leave read_loop;
        end if;

        if this_grade = 'A' or this_grade = 'A+' then
            set this_a_aplus_num = this_a_aplus_num + 1;
            set this_a_class_num = this_a_class_num + 1;
        end if;

        if this_grade = 'A-' then
            set this_a_class_num = this_a_class_num + 1;
        end if;

        if this_grade = 'C+' or this_grade = 'C' or this_grade = 'C-' then
            return -1;
        end if;

    end loop;
    close cur;
    if this_a_aplus_num >= 2 then
        return this_a_class_num;
    end if;
    return -1;
end//
delimiter ;

delimiter //
create function check_B (this_student_id varchar(5))
returns int
begin
    DECLARE done INT DEFAULT FALSE;
    declare this_a_aplus_num int;
    declare this_a_class_num int;
    declare this_c_class_num int;
    declare this_grade varchar(2);

    declare cur CURSOR for select grade from takes natural join student where ID = this_student_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
    set this_a_aplus_num = 0;
    set this_a_class_num = 0;
    set this_c_class_num = 0;

    open cur;

    read_loop: loop
        fetch from cur into this_grade;

        if done then
            leave read_loop;
        end if;

        if this_grade = 'A' or this_grade = 'A+' then
            set this_a_aplus_num = this_a_aplus_num + 1;
            set this_a_class_num = this_a_class_num + 1;
        end if;

        if this_grade = 'A-' then
            set this_a_class_num = this_a_class_num + 1;
        end if;

        if this_grade = 'C+' or this_grade = 'C' or this_grade = 'C-' then
            return -1;
        end if;

    end loop;
    close cur;
    if this_a_aplus_num = 1 then
        return this_a_class_num;
    end if;
    return -1;
end//
delimiter ;

call check_scholarship();
select * from scholarship_temp order by dept_name, level, A_num desc;

drop table scholarship_temp;
drop procedure check_scholarship;
drop function check_A;
drop function check_B;