drop function if exists GETTOTALSALARYBYDEPT;

DELIMITER //
CREATE FUNCTION GETTOTALSALARYBYDEPT(dept_name VARCHAR(20))
RETURNS decimal(20,2) 
BEGIN
    RETURN
        (
        SELECT
            SUM(SALARY) AS TOTAL
        FROM
            (
            SELECT
                *
            FROM
                instructor
            WHERE
                instructor.dept_name = dept_name
        ) AS DEPT_TEACHERS
    );
END//
DELIMITER ;

select dept_name, GETTOTALSALARYBYDEPT(dept_name) from department;

drop function GETTOTALSALARYBYDEPT;